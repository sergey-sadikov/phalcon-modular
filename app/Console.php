<?php


    namespace App;

    use Phalcon\Loader;
    use Phalcon\Cli\Dispatcher;
    use Phalcon\Di\FactoryDefault\Cli as CliDI;

    class Console extends App
    {
        /**
         * Init application
         * */
        public function init(): void
        {
            $this
                -> load()
                -> setDi()
                -> setApplication()
                -> handle();
        }

        public function load()
        {
            $loader = new Loader();
            $loader->registerNamespaces([
                'App\Tasks' => __DIR__.'/tasks'
            ])->register();
            return $this;
        }

        /**
         * Init MVC Application
         */
        public function setApplication(): App
        {
            $this -> application = ( new \Phalcon\Cli\Console($this -> di) );
            return $this;
        }

        /**
         * Init Phalcon Dependency Injection
         */
        public function setDi(): App
        {
            $di = new CliDI();
            $dispatcher = new Dispatcher();
            $di->setShared('dispatcher', $dispatcher);
            $di->get('dispatcher')->setDefaultNamespace('App\Tasks');
            $di->get('dispatcher')->setNamespaceName('App\Tasks');
            $this->di = $di;
            return $this;
        }

        public function handle(): void
        {
            $arguments = [];
            foreach ($_SERVER['argv'] as $k => $arg) {
                if ($k === 1) {
                    $arguments['task'] = $arg;
                } elseif ($k === 2) {
                    $arguments['action'] = $arg;
                } elseif ($k >= 3) {
                    $arguments['params'][] = $arg;
                }
            }
            $this->application->handle($arguments);
        }
    }