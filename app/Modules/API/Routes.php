<?php

    namespace App\Modules\Admin;

    class Routes extends \Phalcon\Mvc\Router\Group
    {
        /**
         * Initialize the router group for the module
         */
        public function initialize()
        {
            // Default paths
            $this -> setPaths(
                [
                    'module'    => 'api',
                    'namespace' => "App\Modules\API\Controllers",
                ]
            );

            $this -> setPrefix('/api');

            $this -> add('/', [
                "controller" => "index",
                "action"     => "index",
            ]);

        }
    }