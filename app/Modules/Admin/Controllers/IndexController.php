<?php
    declare(strict_types=1);

    namespace App\Modules\Admin\Controllers;

    class IndexController extends ControllerBase
    {

        public function indexAction()
        {
            $this->view->pick('index/index');
        }

        public function someAction()
        {
            echo 123;
        }


    }

