<?php
    declare( strict_types = 1 );

    /**
     * This file is part of the Invo.
     *
     * (c) Phalcon Team <team@phalcon.io>
     *
     * For the full copyright and license information, please view
     * the LICENSE file that was distributed with this source code.
     */

    namespace App\Modules\Admin\Controllers;

    use App\Controllers\Controller;

    class ControllerBase extends Controller
    {
        protected function initialize()
        {
//            $this -> tag -> prependTitle('INVO | ');
//            $this -> view -> setTemplateAfter('main');
        }
    }
