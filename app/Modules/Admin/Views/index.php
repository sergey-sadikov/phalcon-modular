<!DOCTYPE html>
<html>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../src/app.css">
    <title>Admin</title>
    </head>
    <body>
    <div class="container">

        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link"  href="/admin/">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link"  href="abc">Page</a>
            </li>
        </ul>

        Admin index -> <?php echo $this->getContent(); ?>
    </div>
    <script src="../src/app.js"></script>
    </body>
</html>
