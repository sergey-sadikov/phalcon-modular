<?php

    namespace App\Modules\Admin;

    class Routes extends \Phalcon\Mvc\Router\Group
    {
        /**
         * Initialize the router group for the module
         */
        public function initialize()
        {
            // Default paths
            $this -> setPaths(
                [
                    'module'    => 'admin',
                    'namespace' => "App\Modules\Admin\Controllers",
                ]
            );

            $this -> setPrefix('/admin');

            $this -> add('/', [
                "controller" => "index",
                "action"     => "index",
            ]);

            $this -> add('/abc', [
                "controller" => "index",
                "action"     => "some",
            ]);

        }
    }