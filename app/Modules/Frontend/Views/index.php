<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/src/app.css">
    </head>
    <body>
        <div class="container">

            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link"  href="/">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="/s">Page</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"  href="/admin/">Admin</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/conf">Config</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="/404">404</a>
                </li>
            </ul>

            Frontend index -> <?php echo $this->getContent(); ?>
        </div>
    <script src="/src/app.js"></script>
    </body>
</html>