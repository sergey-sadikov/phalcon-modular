<?php

    namespace App\Modules\Frontend;

    class Routes extends \Phalcon\Mvc\Router\Group {

        /**
         * Initialize the router group for the module
         */
        public function initialize() {

            // Default paths
            $this -> setPaths(
                [
                    'module'    => 'frontend',
                    'namespace' => "App\Modules\Frontend\Controllers",
                ]
            );

            $this->add('/', [
                "controller" => "index",
                "action"     => "index",
            ]);

            $this->add('/s', [
                "controller" => "index",
                "action"     => "some",
            ]);

            $this->add('/conf', [
                "controller" => "index",
                "action"     => "conf",
            ]);



        }
    }