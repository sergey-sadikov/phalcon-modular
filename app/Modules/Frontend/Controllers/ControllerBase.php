<?php
    declare( strict_types = 1 );

    namespace App\Modules\Frontend\Controllers;

    use App\Controllers\Controller;

    class ControllerBase extends Controller
    {
        protected function initialize(): void
        {
            $this -> tag -> prependTitle('app | ');

//            $this -> view -> setTemplateAfter('index');
        }


    }
