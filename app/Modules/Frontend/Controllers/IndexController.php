<?php
    declare( strict_types = 1 );

    namespace App\Modules\Frontend\Controllers;

    class IndexController extends ControllerBase
    {

        public function initialize(): void
        {
            parent ::initialize();

            $this -> tag -> setTitle('Title');
        }

        public function indexAction(): void
        {
            $this -> view -> pick('index/index');
        }

        public function someAction(): void
        {
            $this -> view -> pick('index/someTemplate');
        }

        public function confAction(): void
        {
            $this -> view -> config = $this -> di[ 'config' ];
            $this -> view -> pick('index/confTemplate');
        }
    }

