<?php

namespace App\Providers;


/**
 * This file is part of the App.
 *
 * (c) Phalcon Team <team@phalcon.io>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use Phalcon\Mvc\Router;
use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;

/**
 * Register the flash service with custom CSS classes
 */
class RouterServiceProvider implements ServiceProviderInterface
{
    public function register(DiInterface $di): void
    {
        $router = dirname(__DIR__).'/routes.php';
        $di->setShared('router', function () use ($router) {
                return include_once $router;
            }
        );
    }
}
