<?php

    namespace App\Controllers;

    use Phalcon\Mvc\Controller as PhalconController;

    class Controller extends PhalconController {

        public function show404Action(): void
        {
            $this->response->setStatusCode(404);
            $this->view->pick('system/404');
        }

        public function show401Action(): void
        {
            $this->response->setStatusCode(401);
        }

        public function show500Action(): void
        {
            $this->response->setStatusCode(500);
        }

    }