<?php

    namespace App;

    use Phalcon\Di\FactoryDefault;
    use Phalcon\Mvc\Application;

    class App
    {

        public $di, $rootPath, $application, $confModules;

        public function __construct()
        {
            $rootPath = realpath('..');
            $this -> rootPath = $rootPath;

            ( \Dotenv\Dotenv ::createImmutable(dirname(__DIR__)) ) -> load();
        }

        /**
         * Init application
         * */
        public function init(): void
        {
            $this
                -> setDi()
                -> setApplication()
                -> registerProviders()
                -> registerModules()
                -> registerRoutes()
                -> handle();
        }

        /**
         * Return application modules config
         * */
        public function getConfModules(): array
        {
            if (!$this -> confModules) {
                $modules = $this -> rootPath . '/config/modules.php';
                if (!file_exists($modules) || !is_readable($modules)) {
                    throw new Exception('File modules.php does not exist or is not readable.');
                }
                $this -> confModules = include_once $modules;
            }
            return $this -> confModules;
        }

        /**
         * Handle request and send response
         */
        public function handle(): void
        {
            $response = $this -> application -> handle($_SERVER[ 'REQUEST_URI' ]);
            $response -> send();
        }

        /**
         * Register Routes
         */
        public function registerRoutes(): App
        {
            $confModules = $this -> getConfModules();
            foreach ($confModules as $moduleName => $conf) {
                $ModuleName = ucfirst($moduleName);
                $moduleRoutesClass = "\App\Modules\\$ModuleName\Routes";
                if (class_exists($moduleRoutesClass)) {
                    $moduleRoutes = new $moduleRoutesClass();
                    $this -> di[ 'router' ] -> mount($moduleRoutes);
                }
            }
            return $this;
        }

        /**
         * Register Modules
         */
        public function registerModules(): App
        {
            $this -> application -> registerModules($this -> getConfModules());
            return $this;
        }

        /**
         * Init MVC Application
         */
        public function setApplication(): App
        {
            $this -> application = ( new Application($this -> di) );
            return $this;
        }

        /**
         * Init Phalcon Dependency Injection
         */
        public function setDi(): App
        {
            $rootPath = $this -> rootPath;
            $this -> di = new FactoryDefault();
            $this -> di -> offsetSet('rootPath', function () use ($rootPath) {
                return $rootPath;
            });
            return $this;
        }

        /**
         * Register Service Providers
         */
        public function registerProviders(): App
        {
            $providers = dirname(__DIR__) . '/config/providers.php';
            if (!file_exists($providers) || !is_readable($providers)) {
                throw new Exception('File providers.php does not exist or is not readable.');
            }

            /** @var array $providers */
            $providers = include_once $providers;
            foreach ($providers as $provider) {
                $this -> di -> register(new $provider());
            }
            return $this;
        }


    }