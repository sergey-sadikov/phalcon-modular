<?php

    use Phalcon\Mvc\Router;

    $router = new Router(false);

    $router -> setDefaults([
        'namespace'  => 'App\Modules\Frontend\Controllers',
        'module'     => 'frontend',
        'controller' => 'Index',
        'action'     => 'show404'
    ]);

//    $router -> notFound();

    return $router;