<?php

    declare( strict_types = 1 );

    namespace App\Tasks;

    use Phalcon\Cli\Task;

    class TestTask extends Task
    {
        public function mainAction()
        {
            echo '— CLI OUTPUT —'.PHP_EOL;
        }

        public function someAction()
        {
            echo '— CLI OUTPUT 2 —'.PHP_EOL;
        }
    }