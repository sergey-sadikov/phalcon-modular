<?php
    declare(strict_types=1);
    use App\App;

    try {

        $rootPath = realpath('..');
        require_once $rootPath . '/vendor/autoload.php';


        (new App())->init();

    } catch (Exception $e) {
        echo $e->getMessage() . '<br>';
        echo '<pre>' . $e->getTraceAsString() . '</pre>';
    }
