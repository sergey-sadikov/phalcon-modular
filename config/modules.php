<?php
    return [
        "admin"  => [
            "className" => App\Modules\Admin\Module::class,
            "path"      => "../app/Modules/Admin/Module.php",
        ],
        "frontend" => [
            "className" => App\Modules\Frontend\Module::class,
            "path"      => "../app/Modules/Frontend/Module.php",
        ],
        "api" => [
            "className" => App\Modules\API\Module::class,
            "path"      => "../app/Modules/API/Module.php",
        ],
    ];
