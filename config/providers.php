<?php

    return [
        \App\Providers\ConfigProvider::class,
        \App\Providers\DatabaseProvider::class,
        \App\Providers\UrlProvider::class,
        \App\Providers\RouterServiceProvider::class,
//                \App\Providers\ViewProvider::class,
        //    \App\Providers\SessionBagProvider::class,
        //    \App\Providers\SessionProvider::class,
        //    \App\Providers\FlashProvider::class,
        //    \App\Providers\DispatcherProvider::class,
    ];
